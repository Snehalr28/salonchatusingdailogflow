//
//  ViewController.swift
//  RecastAI
//
//  Created by plieb on 03/29/2016.
//  Copyright (c) 2017 plieb. All rights reserved.
//
import ApiAI
import UIKit
/**
Class ViewController Example of implementations for Text & Voice Requests
 */
class ViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextViewDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    
    @IBOutlet weak var const_bottombtn: NSLayoutConstraint!
    
    @IBOutlet weak var const_bottom: NSLayoutConstraint!
    @IBOutlet weak var tbl_chat: UITableView!
    
    var defaultSugg = ["Hello","i want to book an appointment?","Haircut","12 August","10 am","Anil"]
    var SuggesionArr = ["Templates","Games","Players","Colours","Days"]
    var gamesArr = ["Cricket","Football","Tenis","Hockey"]
    var playerArr = ["Sachin Tendulkar","Saurabh Ganguly","Ronaldo","Maria Sharapova"]
    var cloursArr = ["Blue","Black","Red","Yellow","Pink"]
    var Days = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"]
    //Outlets
    @IBOutlet weak var textViewChat: UITextView!
    @IBOutlet weak var requestTextField: UITextField!
    var chatArray = NSMutableArray()
    //Vars
    //var bot : RecastAIClient?
    var selectedIndex1 = 0
    var chatBool = false
    override func viewDidLoad()
    {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
     //   self.bot = RecastAIClient(token :"d7e9b7bfe1abef6f2e7ac43e3ffaf27c",language:"en")
        self.title = "Salon Chat Bot"
//        self.bot = RecastAIClient(token : "YOUR_TOKEN", language: "en")
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(sender:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(sender:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)

    }
    @objc func keyboardWillShow(sender: NSNotification) {
       // self.view.frame.origin.y = -150 // Move view 150 points upward
        self.const_bottom.constant = 255
        self.const_bottombtn.constant = 255

        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 1136:
                print("iPhone 5 or 5S or 5C")
            case 1334:
                print("iPhone 6/6S/7/8")
            case 1920, 2208:
                print("iPhone 6+/6S+/7+/8+")
            case 2436:
                print("iPhone X")
                self.const_bottom.constant = 300
                self.const_bottombtn.constant = 300

            default:
                print("unknown")
            }
        }
    }
    
    @objc func keyboardWillHide(sender: NSNotification) {
       // self.view.frame.origin.y = 0 // Move view to original position
        self.const_bottom.constant = 0
        self.const_bottombtn.constant = 0

    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        DispatchQueue.main.async {
            self.tbl_chat.reloadData()
            
        }
        
        let dict = ["message":"Welcome to Salon Cloud Plus how can i help you.",
                    "type":"user"]
        self.chatArray.add(dict)
        self.textViewChat.layer.cornerRadius = 8
        self.textViewChat.layer.borderWidth = 1
        self.textViewChat.layer.borderColor = UIColor.gray.cgColor
        self.textViewChat.keyboardType = .default
    }
    
    
    /**
     Method called when the request was successful
     
     - parameter response: the response returned from the Recast API
     
     - returns: void
     */
    
    
    /**
     Method called when the converse request was successful
     
     - parameter response: the response returned from the Recast API
     
     - returns: void
     */
//    func recastRequestDone(_ response : ConverseResponse)
//    {
//        print(response.replies as Any)
//       // print(response.action?. as Any)
//    }
    
    /**
     Method called when the request failed
     
     - parameter error: error returned from the Recast API
     
     - returns: void
     */
//    func recastRequestError(_ error : Error)
//    {
//        print("Error : \(error)")
//    }
    
    /**
     Make text request to Recast.AI API
     */
    @IBAction func makeRequest()
    {
        if (!(self.requestTextField.text?.isEmpty)!)
        {
            let url = URL(string: self.requestTextField.text!)!
            //Call makeRequest with string parameter to make a text request
            // self.bot?.analyseFile(url, successHandler: recastRequestDone, failureHandle: recastRequestError)
//            self.bot?.textRequest(self.requestTextField.text!, successHandler: { (response) in
//                print(response)
//
//            }, failureHandle: { (error) in
//
//            })    }
        }
        
    }
    
    
    @IBAction func sendChat(_ sender: Any) {
        let dict = ["message":textViewChat.text!,
                    "type":"customer"]
       self.chatArray.add(dict)
        self.chatBool = true
     //   progress.show
//        let loadingNotification = MBProgressHUD.showAdded(to: view, animated: true)
//        loadingNotification.mode = MBProgressHUDMode.indeterminate
//        loadingNotification.label.text = "Loading"
        if (!(self.textViewChat.text?.isEmpty)!)
        {
            let hud = MBProgressHUD.showAdded(to: self.view.window!, animated: true)
            
            self.textViewChat?.resignFirstResponder()
            
            let request = ApiAI.shared().textRequest()
            
            if let text = self.textViewChat?.text {
                request?.query = [text]
            } else {
                request?.query = [""]
            }
            
            request?.setMappedCompletionBlockSuccess({ (request, response) in
                let response = response as! AIResponse
                if response.result.action == "money" {
                    if let parameters = response.result.parameters as? [String: AIResponseParameter]{
                        let amount = parameters["amout"]!.stringValue
                        let currency = parameters["currency"]!.stringValue
                        let date = parameters["date"]!.dateValue
                        
                        print("Spended \(amount) of \(currency) on \(date)")
                    }
                }
            }, failure: { (request, error) in
                // TODO: handle error
            })
            
            request?.setCompletionBlockSuccess({[unowned self] (request, response) -> Void in
                let resultNavigationController = self.storyboard?.instantiateViewController(withIdentifier: "ResultViewController") as! ResultNavigationController
                
                //let response = response as AnyObject?
                if let dict = response as? Dictionary<String,AnyObject> {
                    print("true")
                    let result = dict["result"] as! NSDictionary
                    print(result)
                    let fulfillment = result.value(forKey:"fulfillment") as! NSDictionary
                    let messages = fulfillment.value(forKey:"messages") as! [NSDictionary]
                    for obj in messages{
                        let dict = ["message":obj["speech"],
                                    "type":"user"]

                        self.chatArray.add(dict)
                    }
                    DispatchQueue.main.async {
                        self.textViewChat.text = ""
                        self.tbl_chat.reloadData()
                        
                    }
                }
                resultNavigationController.response = response as AnyObject?
                
//                if let parameters = response?.result.parameters as? [String: AIResponseParameter]{
//                    let result = parameters["result"]!.dictionaryWithValues(forKeys:["fulfillment"])
//                    let currency = parameters["currency"]!.stringValue
//                    let date = parameters["date"]!.dateValue
//
//                    print("Spended \(result) of \(currency) on \(date)")
//                }
              //  self.present(resultNavigationController, animated: true, completion: nil)
                
                hud.hide(animated: true)
                }, failure: { (request, error) -> Void in
                    hud.hide(animated: true)
            });
            
            ApiAI.shared().enqueue(request)
        }
       
        
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    /**
     Make File request to Recast.AI API
     */
    func makeFileRequest()
    {
        if (!(self.requestTextField.text?.isEmpty)!)
        {
            let url = URL(string: self.requestTextField.text!)!
            //Call makeRequest with string parameter to make a text request
           // self.bot?.analyseFile(url, successHandler: recastRequestDone, failureHandle: recastRequestError)
//            self.bot?.textRequest(self.requestTextField.text!, successHandler: { (response) in
//                print(response)
//
//
//            }, failureHandle: { (error) in
//
//            })
        }
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return chatArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let dict = chatArray[indexPath.row] as? NSDictionary ?? [:]
        if dict["type"] as! String == "user"{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ChatBotCell") as! ChatBotCell
            cell.lbl_bot.text = dict["message"] as? String ?? ""
           // cell.lbl_bot.layer.borderColor = UIColor.lightGray.cgColor
            cell.lbl_bot.layer.cornerRadius = 5
            cell.lbl_bot.clipsToBounds = true
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ChatUserCell") as! ChatUserCell
            cell.lbl_user.text = dict["message"] as? String ?? ""
          //  cell.lbl_user.layer.borderColor = UIColor.lightGray.cgColor
             cell.lbl_user.layer.cornerRadius = 5
            cell.lbl_user.clipsToBounds = true
            return cell
        }
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5
    }
   
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier:"SuggesionCollCell", for:indexPath) as! SuggesionCollCell
        cell.lbl_suggesion.adjustsFontSizeToFitWidth = true
        cell.lbl_suggesion.layer.borderColor = UIColor.lightGray.cgColor
        cell.lbl_suggesion.layer.borderWidth = 1
        cell.lbl_suggesion.layer.cornerRadius = 8
        cell.lbl_suggesion.text = SuggesionArr[indexPath.row]
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width:(collectionView.frame.width)/2, height: 50)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
            selectedIndex1 = indexPath.row
            self.addPickerView()
            
        
    }
    func addPickerView(){
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "PickerViewVC") as! PickerViewVC
        vc.view.backgroundColor = UIColor.clear
        vc.hidesBottomBarWhenPushed = true
        self.modalPresentationStyle = .currentContext
        if #available(iOS 8.0, *){
            vc.providesPresentationContextTransitionStyle  = true
            vc.definesPresentationContext = true
        }
        vc.hidesBottomBarWhenPushed = true
        if self.selectedIndex1 == 0{
            vc.pickerData = defaultSugg as NSArray
        }else if selectedIndex1 == 1{
            vc.pickerData = gamesArr as NSArray
        }else if selectedIndex1 == 2{
            vc.pickerData = playerArr as NSArray
        }else if selectedIndex1 == 3{
            vc.pickerData = cloursArr as NSArray
        }else if selectedIndex1 == 4{
            vc.pickerData = Days as NSArray
        }
        vc.CallbackOption { (IsDonePressed, SelectedIndex) in
            self.tabBarController?.tabBar.isHidden = false
            if IsDonePressed{
                if self.selectedIndex1 == 0{
              self.textViewChat.text = self.defaultSugg[SelectedIndex]
                    
                }else if self.selectedIndex1 == 1{
                    self.textViewChat.text = self.gamesArr[SelectedIndex]

                }else if self.selectedIndex1 == 2{
                    self.textViewChat.text = self.playerArr[SelectedIndex]

                }else if self.selectedIndex1 == 3{
                    self.textViewChat.text = self.cloursArr[SelectedIndex]

                }else if self.selectedIndex1 == 4{
                    self.textViewChat.text = self.Days[SelectedIndex]

                }
                
            }
        }
        //        self.tabBarController?.tabBar.isHidden = true
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.present(vc, animated: true, completion: nil)
    }
}
