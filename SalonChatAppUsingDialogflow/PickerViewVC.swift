//Default Picker view for selecting data
//  PickerViewVC.swift
//  Eenadu
//
//  Created by Selva Mac on 26/09/16.
//  Copyright © 2016 Switch Soft Technologies. All rights reserved.
//

import UIKit
typealias SelectedOption = (Bool,NSInteger) -> Void

class PickerViewVC: UIViewController,UIPickerViewDelegate,UIPickerViewDataSource {
    var option : SelectedOption!
    var districtSelectstr = 0
    var index = 0
    var pickerData:NSArray = []
    @IBOutlet var optionsView: UIView!
    @IBOutlet var hgt_bottom: NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.isOpaque = true
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissViewOnTouch))
        view.addGestureRecognizer(tap)
//        if UIDevice.IS_IPHONE_4_OR_LESS{
//            self.hgt_bottom.constant = 50
//        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        self.tabBarController?.tabBar.isHidden = false
    }
    @objc func dismissViewOnTouch() {
        self.dismiss(animated: true, completion: nil)
        option(false,districtSelectstr)
    }
    @IBAction func donePickerMethode(){
        self.dismiss(animated: true, completion: nil)
        option(true,districtSelectstr)
        // self.tabBarController?.tabBar.isHidden = false
    }
    //MARK:-For Getting Index
    func CallbackOption(complition :  @escaping SelectedOption) {
        option = complition
    }
    @IBAction func cancelButtonClicked(_ sender: AnyObject) {
        self.dismiss(animated: true, completion: nil)
        option(false,districtSelectstr)
    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count
    }
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 40
    }
    func pickerView(_ pickerView: UIPickerView, widthForComponent component: Int) -> CGFloat {
        return self.view.frame.size.width
    }
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        let titleData = pickerData[row]
        let myTitle = NSAttributedString(string: titleData as! String, attributes: [NSAttributedStringKey.font:UIFont(name: "Georgia", size: 15.0)!,NSAttributedStringKey.foregroundColor:UIColor.black])
        return myTitle
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        districtSelectstr = row
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
